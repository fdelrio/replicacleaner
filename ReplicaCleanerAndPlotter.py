import numpy as np
import ROOT
import math as m
import argparse
import os
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetStyle("ATLAS")
ROOT.gStyle.SetPalette(ROOT.kRainBow)
l=ROOT.TLatex()
l.SetNDC()
a=ROOT.TLatex()
a.SetNDC()
a.SetTextFont(72)
p=ROOT.TLatex()
p.SetNDC()
p.SetTextFont(42)
v=ROOT.TLatex()
v.SetNDC()
v.SetTextFont(42)
r=ROOT.TLatex()
r.SetNDC()
r.SetTextFont(42)

def PreCleaning(th2b,binx,biny):
  nrep = th2b.GetNReplica()
  exceptions = []
  for i in range(nrep):
    if abs(th2b.GetReplica(i).GetBinContent(binx,biny))>0.9:
      exceptions.append(i)
  return exceptions


def GetBootstrapMeanAndRMSWithExceptions(th2b,binx,biny,exceptions):
  nrep = th2b.GetNReplica()
  
  mean = 0
  for i in range(nrep):
    if i not in exceptions:
      mean+=th2b.GetReplica(i).GetBinContent(binx,biny)
  mean/=nrep

  rms = 0
  val = 0
  for i in range(nrep):
    if i not in exceptions:
      val = th2b.GetReplica(i).GetBinContent(binx,biny)
      rms += val**2
  rms = m.sqrt(abs(rms/nrep-mean**2))

  return mean,rms

def GetExceptionsIteratively(th2b,binx,biny,giveFinalMeanRMS=True,prexceptions=[],nsigma=3):
  done = False
  nrep = th2b.GetNReplica()
  exceptions = prexceptions
  iterations = 0
  while not done:
    done = True
    mean,rms = GetBootstrapMeanAndRMSWithExceptions(th2b,binx,biny,exceptions)
    if rms==0: break
    for i in range(nrep):
      if i in exceptions: continue
      if abs(th2b.GetReplica(i).GetBinContent(binx,biny)-mean)/rms>=nsigma:
	exceptions.append(i)
	done = False
    iterations+=1
    if iterations>=2:
      done = True
  if giveFinalMeanRMS:
    return exceptions,mean,rms
  else:
    return exceptions
    print exceptions
    
def GetExceptionsOneLess(th2b,binx,biny,giveFinalMeanRMS=True,prexceptions=[]):
  done = False
  nrep = th2b.GetNReplica()
  exceptions = prexceptions
  while not done:
    done = True
    mean,rms = GetBootstrapMeanAndRMSWithExceptions(th2b,binx,biny,exceptions)
    if rms==0: break
    maxdist = 0
    worstrep = None
    for i in range(nrep):
      if i in exceptions: continue
      distance = abs(th2b.GetReplica(i).GetBinContent(binx,biny)-mean)
      if distance>maxdist:
        maxdist = distance
        worstrep = i
    newmean,newrms = GetBootstrapMeanAndRMSWithExceptions(th2b,binx,biny,exceptions+[worstrep])
    if abs(maxdist-newmean)/newrms>3:
      exceptions.append(worstrep)
      done = False
  if giveFinalMeanRMS:
    return exceptions,mean,rms
  else:
    return exceptions

def main():
  parser = argparse.ArgumentParser(description="%prog [options]%",formatter_class = argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("--inFile",dest='inFile',help='Input file')
  parser.add_argument("--hname",dest='hname',help='Name of histogram within file')
  parser.add_argument("--method",dest='method',default=0,help="Method for filtering, 0 for iterative (default) and 1 for 1-less")
  parser.add_argument("--outFile",dest='outFile',default='myfile.root',help='Name of output file, default is myfile.root')
  parser.add_argument("--outHist",dest='outHist',default='',help='Name of output histogram, default is same as input')
  parser.add_argument("--inputList",dest='inputList',action='store_true',default=False)
  parser.add_argument("--debug",dest='debug',action='store_true',default=False)
  parser.add_argument("--isNotRelativeUncert",dest='iNRU',action='store_true',default=False,help='set when the input Histogram is NOT the final relative uncertainty')
  parser.add_argument("--nSigma",dest='nSigma',default=3,help='Number of sigmas to filter, default is 3')
  args = parser.parse_args()
  if not args.inFile.endswith('root'):
    if args.inFile.endswih('txt') and not args.inputList:
      print 'For using txt files set --inputList, exiting'
      exit(1)
    elif not args.inFile.endswith('txt'):
      print "Input file is neither a rootfile or a txt, exiting"
      exit(1)
  inFile = []
  if args.inputList:
    fileList = open(args.inFile,'w')
    for line in fileList.readlines():
      inFile.append(line)
  else:
    inFile.append(args.inFile)
  fixedth2s = []
  for File in inFile:
    f = ROOT.TFile(args.inFile)
    th2b = ROOT.TH2FBootstrap()
    f.GetObject("probe_insituJER_toys", th2b)
    nx = th2b.GetNominal().GetNbinsX()
    ny = th2b.GetNominal().GetNbinsY()
    fixedth2 = th2b.GetNominal().Clone()
    if args.outHist: 
      fixedth2.SetName(args.outHist)
      fixedth2.SetTitle(args.outHist)
    if args.inputList:
      fixedth2.SetName(fixedth2.GetName()+File.split('/')[-1])
      fixedth2.SetTitle(fixedth2.GetTitle()+File.split('/')[-1])
    for x in range(nx):
      if args.debug: print 'pt',x+1
      for y in range(ny):
	if args.debug: print 'eta',y+1
	if not args.iNRU: pres = PreCleaning(th2b,x+1,y+1)
	else: pres = []
	if args.debug: print len(pres),' pre-cleaned replicas'
        if not args.method:
	  exceptions,mean,rms = GetExceptionsIteratively(th2b,x+1,y+1,prexceptions=pres,nsigma=nSigma)
        else:
	  exceptions,mean,rms = GetExceptionsOneLess(th2b,x+1,y+1)
        fixedth2.SetBinError(x+1,y+1,rms)
	if args.debug: print len(exceptions),'bad replicas'
  fixedth2s.append(fixedth2)  
  newf = ROOT.TFile(args.outFile,"update")
  etaBin = np.array(["0.0<|#eta|<0.2","0.2<|#eta|<0.7","0.7<|#eta|<1.0","1.0<|#eta|<1.3","1.3<|#eta|<1.8","1.8<|#eta|<2.5","2.5<|#eta|<3.0"])
  for h2 in fixedth2s:
    h2.Write()
    c = ROOT.TCanvas()
    varName = os.path.basename(args.inFile)
    var = str(varName).replace(".root","",1)
    fileName ="FixedRatio_"+var+".pdf"
    c.Print(fileName+"[")
    c.Clear()
    for i in range(1,8):
        h = h2.ProjectionX("",i,i)
        h.GetYaxis().SetTitle("Relative uncertainty")
        h.GetYaxis().SetRangeUser(-0.5,0.5)
        h.GetXaxis().SetTitle("pT [GeV]")
        h.GetXaxis().SetRangeUser(60,2000)
        c = ROOT.TCanvas()
        c.cd
        h.Draw("PE")
        delx = 0.115*696*ROOT.gPad.GetWh()/(427*ROOT.gPad.GetWw())
        c.SetLogx()
        c.SetGridy()
        a.DrawLatex(0.2,0.9,"ATLAS")
        p.DrawLatex(0.2+delx,0.9,"Internal")
        l.DrawLatex(0.2,0.85,etaBin[i-1])
        v.DrawLatex(0.2,0.8,var.replace("AntiKt6LCTopo_","",1))
        r.DrawLatex(0.2,0.75,"R=0.6")
        c.Print(fileName)


  c.Print(fileName+"]")

main()

